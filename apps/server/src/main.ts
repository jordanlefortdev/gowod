import * as express from 'express';
import { connectMongoDB } from './providers/db/mongodb';

connectMongoDB();
const app = express();

app.get('/api', (req, res) => {
  res.send({ message: 'Welcome to server!' });
});

const port = process.env.SERVER_PORT || 3333;
const server = app.listen(port, () => {
  
});

server.on('error', console.error);
