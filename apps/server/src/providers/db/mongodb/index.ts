const mongoose = require('mongoose');

const { DB_PORT, DB_HOST, DB_NAME } = process.env;

export const connectMongoDB = () : void => mongoose.connect(`mongodb://${DB_HOST}:${DB_PORT}/${DB_NAME}`);
