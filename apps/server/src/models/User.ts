const mongoose = require('mongoose');
const { Schema } = mongoose;

const userSchema = new Schema({
  email:  {
      required: true,
      type: String,
      unique: true,
  }, 
  password: String,
  firstname:  String,
  lastname:  String,
  age: Number,
  gender: {
      type: String,
      enum: ['MALE', 'FEMALE'],
  },
  created_date: { type: Date, default: Date.now },
  updated_date: { type: Date, default: Date.now },
  
});

const User = mongoose.model('User', userSchema);